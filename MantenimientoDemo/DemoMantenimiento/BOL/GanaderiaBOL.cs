﻿using DemoMantenimiento.DAL;
using MantenimientoDemo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoMantenimiento.BOL
{
    public class GanaderiaBOL
    {
        public List<EGanaderia> CargarTodo() 
        {
            return new GanaderiaDAL().CargarTodo();
        }
    }
}
