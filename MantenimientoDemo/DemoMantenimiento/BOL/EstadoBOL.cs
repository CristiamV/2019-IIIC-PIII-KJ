﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DemoMantenimiento.DAL;
using MantenimientoDemo.Entities;

namespace DemoMantenimiento.BOL
{
    public class EstadoBOL
    {
        public List<EEstado> CargarTodo(EGenero gen)
        {
            return new EstadoDAL().CargarTodo(gen);
        }
    }
}
