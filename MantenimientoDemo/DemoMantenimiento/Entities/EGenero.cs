﻿namespace MantenimientoDemo.Entities
{
    public class EGenero
    {
        public int Id { get; set; }
        public string Genero { get; set; }
        public char Simbolo { get; set; }
        public bool Activo { get; set; }

        public override string ToString()
        {
            return Genero;
        }
    }
}