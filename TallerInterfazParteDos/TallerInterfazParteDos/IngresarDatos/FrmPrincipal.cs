﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TallerInterfazParteDos.IngresarDatos
{
    public partial class FrmPrincipal : Form
    {
        public FrmPrincipal()
        {
            InitializeComponent();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtNombre.Text.Trim()))
            {
                FrmIngresarDatos frm = new FrmIngresarDatos { Nombre = txtNombre.Text };
                Hide();
                frm.ShowDialog();
                Show();
                lblCorreo.Text = frm.Correo;
                lblTelefono.Text = frm.Telefono;
                lblProvincia.Text = frm.Provincia;
                lblCanton.Text = frm.Canton;
                lblDistrito.Text = frm.Distrito;
                lblCodigoPostal.Text = frm.CodigoPostal.ToString();
            }
            else
            {
                MessageBox.Show("Nombre Requerido!!!");
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            FrmAcercaDe frm = new FrmAcercaDe { Nombre = "AllanDeveloper", Correo = "ventas@allandeveloper.cr" };
            frm.ShowDialog();
        }

        private void FrmPrincipal_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Owner != null) { Owner.Show(); }
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtNombre.Text.Trim()))
            {
                Persona p = new Persona { Nombre = txtNombre.Text.Trim() };
                FrmIngresarDatosClase frm = new FrmIngresarDatosClase { Persona = p };
                Hide();
                frm.ShowDialog();
                Show();
                lblCorreo.Text = p.Correo;
                lblTelefono.Text = p.Telefono;
                lblProvincia.Text = p.Provincia;
                lblCanton.Text = p.Canton;
                lblDistrito.Text = p.Distrito;
                lblCodigoPostal.Text = p.CodigoPostal.ToString();
            }
            else
            {
                MessageBox.Show("Nombre Requerido!!!");
            }
        }
    }
}
