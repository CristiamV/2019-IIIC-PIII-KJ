﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgramacionCapas.Entities
{
    public class EJugador
    {
        public int Id { get; set; }
        public string Correo { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int Pin { get; set; }
        public bool Activo { get; set; }
        public EPuntaje PuntajeActual { get; set; }

        public EJugador()
        {
            PuntajeActual = new EPuntaje();
        }
    }
}
