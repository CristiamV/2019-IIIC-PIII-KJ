﻿using ProgramacionCapas.BOL;
using ProgramacionCapas.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProgramacionCapas.GUI
{
    public partial class FrmRegistro : Form
    {
        public LogicaBOL Logica { get; set; }


        public FrmRegistro()
        {
            InitializeComponent();
        }

        private void FrmRegistro_Load(object sender, EventArgs e)
        {
            txtCorreo1.Text = "lineth.matamoros@gmail.com";
            txtCorreo2.Text = "allanmual@gmail.com";
            txtPin1.Text = "7777";
            txtPin2.Text = "1522";
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Logica.JugadorUno = new EJugador
            {
                Id = Logica.JugadorUno != null ? Logica.JugadorUno.Id : 0,
                Activo = true,
                Apellido = txtApellido1.Text.Trim(),
                Correo = txtCorreo1.Text.Trim(),
                Nombre = txtNombre1.Text.Trim(),
                Pin = Int32.Parse(txtPin1.Text)
            };

            Logica.Registrar(Logica.JugadorUno);
            Console.WriteLine(Logica.JugadorUno.Id);



            btnAceptarUno.Enabled = false;
            if (!btnAceptarUno.Enabled && !btnAceptarDos.Enabled)
            {
                Close();
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            Logica.JugadorDos = new EJugador
            {
                Id = Logica.JugadorDos != null ? Logica.JugadorDos.Id : 0,
                Activo = true,
                Apellido = txtApellido2.Text.Trim(),
                Correo = txtCorreo2.Text.Trim(),
                Nombre = txtNombre2.Text.Trim(),
                Pin = Int32.Parse(txtPin2.Text)
            };
            Logica.Registrar(Logica.JugadorDos);

            btnAceptarDos.Enabled = false;
            if (!btnAceptarUno.Enabled && !btnAceptarDos.Enabled)
            {
                Close();
            }
        }

        private void TxtPin1_TextChanged(object sender, EventArgs e)
        {
            btnAceptarUno.Enabled = true;
        }

        private void TxtPin2_TextChanged(object sender, EventArgs e)
        {
            btnAceptarDos.Enabled = true;
        }

        private void BtnCargarUno_Click(object sender, EventArgs e)
        {
            Logica.JugadorUno = new EJugador
            {
                Correo = txtCorreo1.Text.Trim(),
                Pin = Convert.ToInt32(txtPin1.Text.Trim())
            };
            if (Logica.CargarJugador(Logica.JugadorUno))
            {
                txtNombre1.Text = Logica.JugadorUno.Nombre;
                txtApellido1.Text = Logica.JugadorUno.Apellido;

                btnAceptarUno.Enabled = false;
                if (!btnAceptarUno.Enabled && !btnAceptarDos.Enabled)
                {
                    Close();
                }
            }
        }

        private void BtnCargarDos_Click(object sender, EventArgs e)
        {
            Logica.JugadorDos = new EJugador
            {
                Correo = txtCorreo2.Text.Trim(),
                Pin = Convert.ToInt32(txtPin2.Text.Trim())
            };
            if (Logica.CargarJugador(Logica.JugadorDos))
            {
                txtNombre2.Text = Logica.JugadorDos.Nombre;
                txtApellido2.Text = Logica.JugadorDos.Apellido;

                btnAceptarDos.Enabled = false;
                if (!btnAceptarUno.Enabled && !btnAceptarDos.Enabled)
                {
                    Close();
                }
            }
        }
    }
}
