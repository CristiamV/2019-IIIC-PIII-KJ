create table reproductor.canciones(
	id serial primary key, 
	numero int default 0,
	cancion text not null, 
	album text, 
	artista text, 
	categoria text,
	datos bytea
);
create table reproductor.listas(
	id serial primary key,
	nombre text not null
);
create table reproductor.lista_cancion(
	id serial primary key,
	id_lista int,
	id_cancion int, 
	 CONSTRAINT fk_liscan_lis FOREIGN KEY (id_lista)
        REFERENCES reproductor.listas(id),
	CONSTRAINT fk_liscan_can FOREIGN KEY (id_cancion)
        REFERENCES reproductor.canciones(id)
);

select  id, numero, cancion, album, artista,categoria, datos from reproductor.canciones
order by categoria, artista, album, numero 

