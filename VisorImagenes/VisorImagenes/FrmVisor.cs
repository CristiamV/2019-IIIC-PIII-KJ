﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;
using VisorImagenes.BOL;
using VisorImagenes.Entities;

namespace VisorImagenes
{
    public partial class FrmVisor : Form
    {
        private List<EImagen> imagenes;
        private ImagenBOL ibol;
        private int next;
        private EImagen img;

        public FrmVisor()
        {
            InitializeComponent();
            ibol = new ImagenBOL();
            next = 0;
            timer1.Start();
        }

        private void Bajar()
        {
            if (panel1.Height + panel1.Bounds.Y < Height)
            {
                for (int i = 0; i < 40; i++)
                {
                    panel1.SetBounds(panel1.Bounds.X, panel1.Bounds.Y + 1, panel1.Width, panel1.Height);
                    Thread.Sleep(2);
                    Refresh();
                }
            }
        }

        private void Salio()
        {
            int px = panel1.Bounds.X;
            int py = panel1.Bounds.Y;

            int cx = Cursor.Position.X;
            int cy = Cursor.Position.Y;

            if (cy < py - 30 || cx < px - 30 || cx > px + panel1.Width + 30)
            {
                Bajar();
            }
            else
            {
                Subir();
            }
        }

        private void Subir()
        {
            if (panel1.Height + panel1.Bounds.Y > Height)
            {
                for (int i = 0; i < 40; i++)
                {
                    panel1.SetBounds(panel1.Bounds.X, panel1.Bounds.Y - 1, panel1.Width, panel1.Height);
                    Thread.Sleep(2);
                    Refresh();
                }
            }
        }

        private void FrmVisor_MouseMove(object sender, MouseEventArgs e)
        {
            Salio();
        }

        private void FrmVisor_Load(object sender, EventArgs e)
        {
            imagenes = ibol.Cagar();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            FrmAgregar frm = new FrmAgregar();
            if (frm.ShowDialog() == DialogResult.OK)
            {
                imagenes = ibol.Cagar();
            }
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            img = imagenes[next++];
            notifyIcon1.ShowBalloonTip(1000, img.Titulo, img.Tipo + "\n" + img.Descripcion, ToolTipIcon.Info);
            pictureBox1.Image = img.Imagen;

            if (next >= imagenes.Count)
            {
                next = 0;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            timer1.Stop();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            timer1.Start();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            timer1.Stop();

            DialogResult dlg = MessageBox.Show("¿Esta seguro que desea eliminar la imagen actual(" + img?.Titulo + ")"
                , "Eliminar", MessageBoxButtons.YesNo);
            if (dlg == DialogResult.Yes)
            {
                ibol.Eliminar(img);
                imagenes = ibol.Cagar();
                next = 0;
            }

            timer1.Start();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            FrmAgregar frm = new FrmAgregar();
            frm.Imagen = img;

            if (frm.ShowDialog() == DialogResult.OK)
            {
                imagenes = ibol.Cagar();
            }
            timer1.Start();
        }
    }
}
