﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using VisorImagenes.Entities;

namespace VisorImagenes.DAL
{
    public class ImagenDAL
    {
        //public EImagen CargarImagen(int id)
        //{
        //    try
        //    {
        //        string sql = "select  id, imagen, activo from ganaderia.imagenes where id = @id";

        //        using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
        //        {
        //            con.Open();
        //            NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
        //            cmd.Parameters.AddWithValue("@id", id);
        //            NpgsqlDataReader reader = cmd.ExecuteReader();
        //            if (reader.Read())
        //            {
        //                return CargarImagen(reader);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return null;
        //}

        private EImagen CargarImagen(NpgsqlDataReader reader)
        {
            EImagen img = new EImagen
            {
                Id = reader.GetInt32(0),
                Imagen = reader[1] != DBNull.Value ? Image.FromStream(new MemoryStream((byte[])reader[1])) : null,
                Tipo = reader.GetString(2),
                Titulo = reader.GetString(3),
                Descripcion = reader.GetString(4)
            };
            return img;
        }

        public int InsertarImagen(EImagen imagen)
        {
            try
            {
                string sql = "INSERT INTO visor.imagenes(imagen, titulo,tipo, descripcion)" +
                    " VALUES(@ima, @tit, @tip, @desc) returning id; ";

                using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
                {
                    con.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    MemoryStream ms = new MemoryStream();
                    imagen.Imagen.Save(ms, ImageFormat.Jpeg);
                    cmd.Parameters.AddWithValue("@ima", ms.ToArray());
                    cmd.Parameters.AddWithValue("@tit", imagen.Titulo);
                    cmd.Parameters.AddWithValue("@tip", imagen.Tipo);
                    cmd.Parameters.AddWithValue("@desc", imagen.Descripcion);
                    int id = (int)cmd.ExecuteScalar();
                    return id;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<EImagen> Cargar()
        {
            List<EImagen> imagenes = new List<EImagen>();
            try
            {
                string sql = "select  id, imagen, tipo, titulo, descripcion from visor.imagenes";

                using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
                {
                    con.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    NpgsqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        imagenes.Add(CargarImagen(reader));
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return imagenes;
        }

        public List<String> CargarTipos()
        {

            List<String> tipos = new List<String>();
            try
            {

                string sql = "select distinct tipo from visor.imagenes order by tipo";
                using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
                {
                    con.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    NpgsqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        tipos.Add(reader.GetString(0));
                    }
                }
                return tipos;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EliminarImagen(EImagen img)
        {
            try
            {

                Console.WriteLine(img.Id);
                string sql = "delete from visor.imagenes where id = @id";
                using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
                {
                    con.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    cmd.Parameters.AddWithValue("@id", img.Id);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Actualizar(EImagen imagen)
        {
            try
            {
                string sql = "update visor.imagenes set imagen = @ima, titulo = @tit,tipo=@tip, descripcion=@desc" +
                    " where id = @id";

                using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
                {
                    con.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    MemoryStream ms = new MemoryStream();
                    imagen.Imagen.Save(ms, ImageFormat.Jpeg);
                    cmd.Parameters.AddWithValue("@ima", ms.ToArray());
                    cmd.Parameters.AddWithValue("@tit", imagen.Titulo);
                    cmd.Parameters.AddWithValue("@tip", imagen.Tipo);
                    cmd.Parameters.AddWithValue("@desc", imagen.Descripcion);
                    cmd.Parameters.AddWithValue("@id", imagen.Id);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
