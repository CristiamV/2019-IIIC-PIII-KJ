﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TallerInterfazParteUno
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Sumar(object sender, EventArgs e)
        {
            double n1;
            double n2;
            if (!Double.TryParse(txtOpe1.Text, out n1))
            {
                txtOpe1.Clear();
            }
            if (!Double.TryParse(txtOpe2.Text, out n2))
            {
                txtOpe2.Clear();
            }
            txtResult.Text = String.Format("{0}x{1}={2:F2}", n1, n2, n1 * n2);
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            txtOpe1.Clear();
            txtOpe2.Clear();
            txtResult.Clear();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
